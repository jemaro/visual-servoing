function [ citp1_M_cit ] = velocity2matrix( v )
%v -> citp1_M_cit Exponential Mapping from Lie Algebra to Lie Group
%   v is a 6x1 Column Vector of the form=[v1 v2 v3 w1 w2 w3]' which is
%   defined using 6 Generator Matrices(4x4)
%   each of the six elements on multiplication with the generator matrices
%   as follows give the complete matrix:
%   v = v1*G1 + v2*G2 + v3*G3 + w1*G4 + w2*G5 + w3*G6
%   To map v to citp1_M_cit we need to perform e^(v)
%   This can be done by following the algorithm:
%   Algorithm
%
%
%
%
w=v(4:6);
u=v(1:3);
wx=[0 -w(3) w(2);w(3) 0 -w(1);-w(2) w(1) 0];
theta=sqrt(w'*w);
if(theta~=0),
  A=sin(theta)/theta;
  B=(1-cos(theta))/(theta^2);
  C=(1-A)/(theta^2);
else
  A=0;
  B=0;
  C=0;
end
R=eye(3)+(A*wx)+(B*(wx*wx));
V=eye(3)+B*wx+C*(wx*wx);
Vp=V*u;
citp1_M_cit=zeros(4);
citp1_M_cit(1:3,1:3)=R;
citp1_M_cit(1:3,4)=Vp;
citp1_M_cit(4,4)=1;

end