function [M] = pose2matrix(p)

p = reshape(p, 6, 1);

M = eye(4);
M(1:3,4) = p(1:3);

theta = norm(p(4:6));
p(4:6) = p(4:6) / theta;

M(1:3, 1:3) = cos(theta)*eye(3)+sin(theta)*[0 -p(6) p(5) ; p(6) 0 -p(4) ; -p(5) p(4) 0]+(1-cos(theta))*p(4:6)*p(4:6)';
