clear all
close all
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load data
load imagePoints.txt;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameters initialization
% Intrinsic
alpha_u=640;
alpha_v=640;
u_0=320;
v_0=240;
% Extrinsic
p=[0.01  -0.01  0.01  0.001  0.001  -0.001]';

% Virtual visual servoing loop
error=1000;
epsilon=11.8;
iter=1;
max_iter = 1; % change later

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% desired feature points vector
Us=[]; 

for i=1: size(imagePoints,1)
    Us=[Us; imagePoints(i,1); imagePoints(i,2)];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% image display with detected points
I=imread('objectImage.png');
figure(1)
imshow(I)
hold on
plot(imagePoints(:,1), imagePoints(:,2), 'r+')
hold off

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% virtual visual servoing loop 

while(error>epsilon && iter<max_iter)
    
    iter=iter+1;
end
