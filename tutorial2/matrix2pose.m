function [p] = matrix2pose(M)

R = M(1:3, 1:3);

minimum = 0.0001;

s = (R(2,1)-R(1,2))*(R(2,1)-R(1,2)) ...
    + (R(3,1)-R(1,3))*(R(3,1)-R(1,3)) ...
    + (R(3,2)-R(2,3))*(R(3,2)-R(2,3));
s = sqrt(s)*0.5;
c = (R(1,1)+R(2,2)+R(3,3)-1.0)*0.5;
theta=atan2(s,c);  % /* theta in [0, PI] since s > 0 */

if(theta < minimum)
    tu = [0 0 0];
else

% // General case when theta != pi. If theta=pi, c=-1
if ( (1+c) > minimum) %// Since -1 <= c <= 1, no fabs(1+c) is required
    sincar = s/theta; %sinus cardinal
    tu(1) = (R(3,2)-R(2,3))/(2*sincar);
    tu(2) = (R(1,3)-R(3,1))/(2*sincar);
    tu(3) = (R(2,1)-R(1,2))/(2*sincar);
else %/* theta near PI */
    if ( (R(1,1)-c) < eps )
        tu(1) = 0.0;
    else
        tu(1) = theta*(sqrt((R(1,1)-c)/(1-c)));
    end
    if ((R(3,2)-R(2,3)) < 0)
        tu(1) = -tu(1);
    end
    
    if ( (R(2,2)-c) < eps )
        tu(2) = 0.0;
    else
        tu(2) = theta*(sqrt((R(2,2)-c)/(1-c)));
    end
    if ((R(1,3)-R(3,1)) < 0)
        tu(2) = -tu(2);
    end
    
    if ( (R(3,3)-c) < eps )
        tu(3) = 0.0;
    else
        tu(3) = theta*(sqrt((R(3,3)-c)/(1-c)));
    end
    
    if ((R(2,1)-R(1,2)) < 0)
        tu(3) = -tu(3);
    end
end

t = M(1:3,4);

p = [t ; tu'];

end